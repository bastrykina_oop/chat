import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class Message implements Serializable {
    private String login;
    private String message;
    //private String[] users;
    private Date time;

    public Message(String log, String msg) {
        login = log;
        message = msg;
        time = Calendar.getInstance().getTime();
    }

    public String getLogin() {
        return login;
    }

    public String getMessage() {
        return message;
    }

    public String getDate() {
        return time.toString();
    }
}

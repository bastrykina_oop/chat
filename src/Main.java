

public class Main {
    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Usage: [-c] for client version, [-s] for server");
            return;
        }
        if (args[0].equals("-s")) {
            Server server = new Server();
            server.listen();
        }
        if (args[0].equals("-c")) {
            Client client = new Client();
            client.start();
        }
        else {
            System.out.println("Usage: [-c] for client version, [-s] for server");
        }
    }
}

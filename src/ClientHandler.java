import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class ClientHandler implements Runnable {
    private Socket socket;
    private Message msg;
    private String login;
    private Client client;
    private static volatile Map<String, Client> userList;
    private static volatile LinkedList<Message> chatHistory;
    static {
        userList = new HashMap<>();
        chatHistory = new LinkedList<>();
    }

    public ClientHandler(Socket s) {
        socket = s;
    }

    public void run() {
        ObjectInputStream inputStream   = null;
        ObjectOutputStream outputStream = null;
        try {
            inputStream   = new ObjectInputStream(socket.getInputStream());
            outputStream = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            msg = (Message)inputStream.readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        if (msg.getMessage().equals("join"))  {
                login = msg.getLogin();
                client = new Client(socket, inputStream, outputStream);
                userList.put(login, client);
                broadcast(msg.getLogin() + " joined the chat\n\r");
                sendChatHistory();
        } else {
            try {
                outputStream.writeObject(new Message("Server", "Need to login!"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        while (true) {
            try {
                msg = (Message)inputStream.readObject();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            if (!userList.containsKey(msg.getLogin())) {
                try {
                    outputStream.writeObject(new Message("Server", "No user with" + msg.getLogin() + "login"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else {
                Client reciever = userList.get(msg.getLogin());
                try {
                    reciever.getOutputStream().writeObject(new Message(login, msg.getMessage()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void sendChatHistory() {
        for (Message m : chatHistory) {
            try {
                client.getOutputStream().writeObject(m);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void broadcast(String bcastMsg) {
        for (HashMap.Entry<String, Client> e : userList.entrySet()) {
            ObjectOutputStream oos = e.getValue().getOutputStream();
            try {
                oos.writeObject(new Message("Server", bcastMsg));
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
}

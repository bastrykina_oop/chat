import java.io.*;
import java.net.Socket;

public class Client {
    private Socket clientSocket;
    private ObjectInputStream ois;
    private ObjectOutputStream oos;
    
    public Client() {
        try {
            clientSocket = new Socket("192.168.183.1", 2049);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            OutputStream os = clientSocket.getOutputStream();
            oos = new ObjectOutputStream(os);
            InputStream is = clientSocket.getInputStream();
            ois = new ObjectInputStream(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Client(Socket s, ObjectInputStream ois, ObjectOutputStream oos) {
        clientSocket = s;
        this.ois = ois;
        this.oos = oos;
    }

    public void start() {
        BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
        String myLogin = "";
        System.out.println("Enter your login:");
        try {
            myLogin = consoleReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            oos.writeObject(new Message(myLogin, "join"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String companionLogin = "";
        System.out.println("Enter companion's login:");
        try {
            companionLogin = consoleReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String line = "";
        while (true) {
            try {
                line = consoleReader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (line.equalsIgnoreCase("exit")) break;

            try {
                oos.writeObject(new Message(companionLogin, line));
            } catch (IOException e) {
                e.printStackTrace();
            }

            Message m = null;
            try {
                m = (Message) ois.readObject();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            System.out.println(m.getDate() + " ["+ m.getLogin() +"] " + m.getMessage());
        }
        try {
            consoleReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Socket getClientSocket() {
        return clientSocket;
    }

    public ObjectInputStream getInputStream() {
        return ois;
    }

    public ObjectOutputStream getOutputStream() {
        return oos;
    }
}

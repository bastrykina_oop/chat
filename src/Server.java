import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    private int port;
    ServerSocket socketListener;

    public Server() {
        try {
            socketListener = new ServerSocket(2049);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        this.port = 2048;
    }

    public void listen() {
        while (true) {
            Socket clientSocket = null;
            try {
                clientSocket = socketListener.accept();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            if (clientSocket == null) continue;
            System.out.println("Got request from " + clientSocket.getInetAddress() +":"+ clientSocket.getPort());
            Thread clientThread = new Thread(new ClientHandler(clientSocket));
            clientThread.start();
        }
    }
}

